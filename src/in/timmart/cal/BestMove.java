package in.timmart.cal;

/**
 * Created by tim on 8/23/15.
 */
public class BestMove {
    private Integer turn;
    private Integer comingFrom;
    private Integer withRollOf;

    public BestMove(Integer turn, Integer comingFrom, Integer withRollOf) {
        this.turn = turn;
        this.comingFrom = comingFrom;
        this.withRollOf = withRollOf;
    }

    public Integer getTurn() {
        return turn;
    }

    public Integer getComingFrom() {
        return comingFrom;
    }

    public Integer getWithRollOf() {
        return withRollOf;
    }

    public String toString() {
        return "BestMove(turn=" + turn + ", comingFrom=" + comingFrom + ", withRollOf=" + withRollOf + ")";
    }
}
