package in.timmart.cal;

import java.util.*;

public class Cal {
    private static final Integer MAX_DICE_ROLL = 6;

    public static List<BestMove> solveBoard(List<Integer> board) {
        board = modelFullBoard(board);
        List<BestMove> bestMoves = new ArrayList<>();
        for (Integer i : board) {
            bestMoves.add(null);
        }
        bestMoves.set(0, new BestMove(0, null, null));
        Deque<Integer> processQueue = new ArrayDeque<>();
        processQueue.push(0);
        Integer iterations = 0;

        while (!processQueue.isEmpty()) {
            iterations++;
            Integer index = processQueue.pop();
            Integer tile = board.get(index);
            BestMove curBestMove = bestMoves.get(index);
            List<Integer> offsets = new ArrayList<>();
            if (tile != 0) {
                Integer next_tile = index + tile;
                if (next_tile <= 0 || next_tile > board.size()) { // include the 0 because that's the non-returnable starting spot
                    return null;
                }
                offsets.add(tile);
            } else {
                Integer max_roll = Math.min(MAX_DICE_ROLL, board.size() - index - 1);
                for (int i = 1; i <= max_roll; i++) {
                    offsets.add(i);
                }
            }
            for (int offset : offsets) {
                int jump = index + offset;
                if (bestMoves.get(jump) != null && bestMoves.get(jump).getTurn() > (curBestMove.getTurn() + 1)) {
                    bestMoves.set(jump, new BestMove(curBestMove.getTurn() + 1, index, offset));
                } else if (bestMoves.get(jump) == null) {
                    bestMoves.set(jump, new BestMove(curBestMove.getTurn() + 1, index, offset));
                    if (jump != board.size() - 1) {
                        processQueue.push(jump);
                    }
                }
            }
        }
        System.out.println("iterations=" + iterations);
        return backtrack_best_moves(bestMoves);
    }

    private static List<BestMove> backtrack_best_moves(List<BestMove> bestMoves) {
        List<BestMove> tracks = new ArrayList<>();
        BestMove move = bestMoves.get(bestMoves.size() - 1);
        if (move == null) {
            return null;
        }
        tracks.add(0, move);
        while (move.getComingFrom() != null) {
            move = bestMoves.get(move.getComingFrom());
            tracks.add(0, move);
        }
        return tracks;
    }

    private static List<Integer> modelFullBoard(List<Integer> terseBoard) {
        List<Integer> board = new ArrayList<>(terseBoard);
        board.add(0, 0);
        board.add(0);
        return board;
    }

    public static void main(String [] args) {
        // i first wrote in Python, and there's much more verification there.
        List<Integer> test =  Arrays.asList(4,0,-2,0,1,1,1,1,1);
        List<BestMove> moves = solveBoard(test);
        if (moves == null) {
            System.out.println("board can't finish");
        } else {
            for (BestMove move: moves) {
                System.out.println(move.toString());
            }
        }
    }
}
